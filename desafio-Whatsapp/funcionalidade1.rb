Funcionalidade: Criar um grupo com contatos pré-selecionados
  Validar se a criação de grupo está funcionando conforme esperado
  Validar o(s) cenarios negativos
  O grupo deve ser criado com os usuarios informados

      Cenario: Criar um grupo
        Dado o celular conectado na internet
          E validado o token de segurança
          E os destinatarios previamente selecionados
        Quando abrir novas mensagens
          E abrir Novo Grupo
          E marcar os usuarios selecionados
          E avançar para a nova tela
          E Inserir o nome do Grupo
          E criar o grupo
        Então o grupo deve ser criado
          E todos os usuarios escolhidos devem estar no grupo



    Cenario: Criar um grupo sem nenhum membro
        Dado o celular conectado na internet
        E validado o token de segurança
      Quando abrir novas mensagens
        E abrir Novo Grupo
        E não selecionar nenhum usuario
        E avançar para a nova tela
      Então uma mensagem deve ser exibida
        E a mensagem "pelo menos 1 contato deve ser selecionado"
        E após 2 segundos a mensagem deve sumir da tela
