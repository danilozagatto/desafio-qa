Funcionalidade: Enviar mensagem para um destinatario pre-determinado
  Validar se o envio de mensagens esta funcionando conforme esperado
  Validar o(s) cenarios negativos
  A mensagem deve ser enviada para o destinatario e o caminho desta mensagem deve ser monitorado

      Cenario: Enviar mensagem e validar entrega ao servidores do Whatsapp
        Dado o celular conectado na internet
          E validado o token de segurança
          E o destinatario estar previamente selecionado
        Quando procurar o destinatario na lista de contatos
          E abrir a janela deste destinatario
          E inserir a mensagem "Teste de Entrega nos servidores Whatsap"
          E clicar no botão de envio
        Então a mensagem deve ser enviada
          E o sinal de envio deve ficar com uma seta cinza indicando a entrega da mensagem nos servidores do Whatsapp

       Cenario: Enviar mensagem e validar entrega ao destinatario
        Dado o celular conectado na internet
          E validado o token de segurança
          E o destinatario estar previamente selecionado
          E o destinatario conectado na internet
        Quando procurar o destinatario na lista de contatos
          E abrir a janela deste destinatario
          E inserir a mensagem "Teste de entrega ao destinatario"
          E clicar no botão de envio
        Então a mensagem deve ser enviada
          E o sinal de envio deve ficar com uma seta cinza
          E o sinal de envio deve ficar com duas setas cinzas indicando entrega ao destinatario

      Cenario: Enviar mensagem e validar mensagem lida pelo destinatario
        Dado o celular conectado na internet
          E validado o token de segurança
          E o destinatario estar previamente selecionado
          E o destinatario online no whatsapp
          E o destinatario conectado na internet
        Quando procurar o destinatario na lista de contatos
          E abrir a janela deste destinatario
          E inserir a mensagem "Teste de leitura da mensagem"
          E clicar no botão de envio
        Então a mensagem deve ser enviada
          E o sinal de envio deve ficar com uma seta cinza
          E o sinal de envio deve ficar com duas setas CINZAS indicando entrega ao destinatario
          E o sinal de envio deve ficar com duas setas AZUIS indicando a leitura da mensagem pelo destinatario

      Cenario: Enviar mensagem sem conexão com a internet
        Dado o celular validado o token de segurança
          E com o whatsapp aberto
          E o destinatario estar previamente selecionado
        Quando procurar o destinatario na lista de contatos
          E abrir a janela deste destinatario
          E inserir a mensagem "Teste de Entrega de mensagem offline"
          E clicar no botão de envio
        Então a mensagem deve ser enviada
          E o sinal de envio deve ficar com um relogio cinza indicando que a mensagem não foi enviada
