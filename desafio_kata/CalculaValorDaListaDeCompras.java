package desafio_kata;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by 609855 on 11/05/2017.
 */
public class CalculaValorDaListaDeCompras {

    public long groupByFrutas(ArrayList<Fruta> frutas) {

        Map<Fruta, Long> listaComprasAgrupada = frutas.stream().collect(
                Collectors.groupingBy(Function.identity(), Collectors.counting()));

        Iterator it = listaComprasAgrupada.entrySet().iterator();
        long valorTotal = 0;
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            Fruta fruta = (Fruta) pair.getKey();
            long quantidade = (long) pair.getValue();
            valorTotal += fruta.calculoDesconto.calculaDesconto(fruta, quantidade);
        }
        return valorTotal;
    }
}

