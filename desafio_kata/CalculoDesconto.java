package desafio_kata;

/**
 * Created by 609855 on 11/05/2017.
 */
public interface CalculoDesconto {
    long calculaDesconto(Fruta fruta, long quantidade);
}
