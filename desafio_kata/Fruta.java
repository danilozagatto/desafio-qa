package desafio_kata;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by 609855 on 11/05/2017.
 */
public class Fruta {

    String nome;
    long preco;
    CalculoDesconto calculoDesconto;

    public Fruta(CalculoDesconto calculoDesconto, long preco) {
        this.preco = preco;
        this.nome = calculoDesconto.toString();
        this.calculoDesconto = calculoDesconto;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Fruta) {
            Fruta other = (Fruta) obj;
            return new EqualsBuilder().append(nome, other.nome).isEquals();
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(nome).toHashCode();
    }
}
