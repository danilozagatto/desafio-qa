package desafio_kata;

/**
 * Created by 609855 on 11/05/2017.
 */
public enum PromocaoFrutas implements CalculoDesconto {
    AMORA {
        @Override
        public long calculaDesconto(Fruta fruta, long quantidade) {
            long restoDivisao = quantidade % 3;
            long valorEspecial = (quantidade / 3) * 130;
            return valorEspecial + (restoDivisao * fruta.preco);
        }
    }, BANANA {
        @Override
        public long calculaDesconto(Fruta fruta, long quantidade) {
            long restoDivisao = quantidade % 2;
            long valorEspecial = (quantidade / 2) * 45;
            return valorEspecial + (restoDivisao * fruta.preco);
        }
    }, CAJU {
        @Override
        public long calculaDesconto(Fruta fruta, long quantidade) {
            return quantidade * fruta.preco;
        }
    }, DAMASCO {
        @Override
        public long calculaDesconto(Fruta fruta, long quantidade) {
            return quantidade * fruta.preco;
        }
    };
}
