package desafio_kata;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static desafio_kata.PromocaoFrutas.*;

/**
 * Created by 609855 on 12/05/2017.
 */
public class TestaTodosValores {

    @Test
    public void testeUm() {
        ArrayList<Fruta> listaFrutas = new ArrayList<>(Arrays.asList(
                new Fruta(AMORA, 50),
                new Fruta(AMORA, 50),
                new Fruta(AMORA, 50),
                new Fruta(AMORA, 50),
                new Fruta(BANANA, 30),
                new Fruta(BANANA, 30),
                new Fruta(BANANA, 30),
                new Fruta(CAJU, 20),
                new Fruta(CAJU, 20),
                new Fruta(DAMASCO, 15)));

        Assert.assertEquals(new CalculaValorDaListaDeCompras().groupByFrutas(listaFrutas), 310);
    }

    @Test
    public void testeDois() {
        ArrayList<Fruta> listaFrutas = new ArrayList<>(Arrays.asList(
                new Fruta(AMORA, 50),
                new Fruta(BANANA, 30),
                new Fruta(CAJU, 20),
                new Fruta(DAMASCO, 15)));

        Assert.assertEquals(new CalculaValorDaListaDeCompras().groupByFrutas(listaFrutas), 115);
    }

    @Test
    public void testeIncremental() {
        ArrayList<Fruta> listaFrutas = new ArrayList<>(Arrays.asList(
                new Fruta(AMORA, 50)));
        Assert.assertEquals(new CalculaValorDaListaDeCompras().groupByFrutas(listaFrutas), 50);
        listaFrutas.add(new Fruta(BANANA, 30));
        Assert.assertEquals(new CalculaValorDaListaDeCompras().groupByFrutas(listaFrutas), 80);
    }
}
